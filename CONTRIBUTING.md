### Simple way

1. Pick an issue from the board (Issues > Board) and move it from "To Do" to "Doing". This is to ensure that multiple people do not do same work. Also you're welcome to create new issues.

2. Download the book as a zip archive and unpack it.

3. Open an HTML-file from directory 'html_en' that you want to work on in your web browser.

4. When you spot an error or you want to modify something, open the HTML-file in a text editor with HTML support (like [Notepad++](https://notepad-plus-plus.org/) or [Atom](https://atom.io/)) and do you change. Some [knowledge of HTML](https://www.w3schools.com/html/default.asp) might help.

5. After you save the file, refresh the page in the browser and verify that the change is in place.

6. After you're done with the change, send the modified files to me and I will publish them.

### Hardcore way

The same as the simple way above but instead of downloading the archive, fork and clone the repo. And instead of sending me modified files directly, push them to your private fork and raise the pull request that I will merge.